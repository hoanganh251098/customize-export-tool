const
    puppeteer   = require('puppeteer'),
    https       = require('https'),
    request     = require('request'),
    clipboardy  = require('clipboardy')
    ;

$('#export-btn').on('click', async function() {
    $('#export-btn').prop('disabled', true);
    setOutput('');
    try {
        await main();
    }
    catch(e) {
        alert(e);
    }
    $('#export-btn').prop('disabled', false);
});
$('#copy-btn').on('click', function() {
    printMessage('Copied.');
    clipboardy.writeSync($('#output').val());
});

function printMessage(msg) {
    const rowsLimit = +$('#message').attr('rows');
    const finalMessage = $('#message').val() + '\n' + msg;
    $('#message').val(finalMessage.split('\n').slice(-rowsLimit).join('\n'));
}

function setOutput(text) {
    $('#output').val(text);
}

async function main() {
    let previewImgURL = $('#preview-url-input').val(),
        browser,
        page,
        hashCode,
        requestConfig,
        jsonData,
        productURL,
        finalUpload
        ;

    try {
        hashCode = previewImgURL.match(/(?<=products-images\/)[a-zA-Z0-9]+/g)[0];
    }
    catch(e) {
        alert('Failed to get hash code');
        return;
    }

    printMessage('Loading user input ...');
    jsonData = await (new Promise((resolve, reject) => {
        request(`http://dev.goodsmize.com/design-upload-v2/item-exports/${hashCode}`, function(error, response, body) {
            if(error) reject(error);
            resolve(JSON.parse(body));
        });
    }));

    printMessage('Loading product page ...');
    productURL  = `https://d1vyizgcxsj7up.cloudfront.net/customize/index.html?product=${jsonData.productName}&mode=debug`;
    browser     = await puppeteer.launch({ headless: false,
    defaultViewport: null, });
    page        = await browser.newPage();
    // await page.setViewport({ width: 1366, height: 768});

    await page.goto(productURL, {waitUntil: 'networkidle2'});

    printMessage('Fetching user input ...');
    await page.evaluate(async (_) => {
        await App.canvas.import(_);
    }, jsonData);
}

